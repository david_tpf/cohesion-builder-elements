<?php
/*
 * ----------------------------------------------------------------------------------
 * Cohesion Plugin.
 * ----------------------------------------------------------------------------------
 * Cohesion Plugin that provides custom functionality made by The Pattern
 * Factory.
 *
 * @wordpress-plugin
 * Plugin Name:       Cohesion Builder Elements
 * Plugin URI:        https://thepatternfactory.co.uk
 * Description:       Builder Elements for Yoo Theme. Created by The Pattern Factory
 * Version:           1.0.0
 * Author:            The Pattern Factory
 * Author URI:        https://thepatternfactory.co.uk
 * License:           GPL version 3 or any later version
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain:       cohesion
 * Domain Path:       /languages
 * ----------------------------------------------------------------------------------
 */

 use Yootheme\Application;

/*
 * ----------------------------------------------------------------------------------
 * Stop Direct Access
 * ----------------------------------------------------------------------------------
 */
if (!defined('WPINC')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit('These are not the scripts you are looking for, move along... move along...');
}

/**
 * ----------------------------------------------------------------------------------
 * Add our Elements
 * ----------------------------------------------------------------------------------
 */
add_action('after_setup_theme', function () {
    if (!class_exists(Application::class, false)) {
        return;
    }

    $app = Application::getInstance();
    $app->load(__DIR__ . '/bootstrap.php');
});